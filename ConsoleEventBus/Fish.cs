﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleEventBus
{
    public class Fish
    {
        public void Fishing()
        {
            //初始化鱼竿
            var fishingRod = new FishingRod();
            //声明垂钓者
            var jeff = new FishingMan("小明");
            //分配鱼竿
            jeff.FishingRod = fishingRod;
            //注册观察者1
            fishingRod.FishingEvent += jeff.Update;
            //注册观察者2
            fishingRod.FishingEvent += jeff.Log;
            //循环钓鱼
            while (jeff.FishCount < 5)
            {
                jeff.Fishing();
                Console.WriteLine("--------------------");
                //Thread.Sleep(5000);
            }
        }
    }

    public class FishingRod
    {
        //声明委托
        public delegate void FishingHandler(FishType type);
        //声明事件
        public event FishingHandler FishingEvent;

        public void ThrowHook(FishingMan man)
        {
            Console.WriteLine("开始下钩了");
            var type = (FishType)new Random().Next(0, 5);
            Console.WriteLine("铃铛：叮叮叮，鱼儿咬钩了");
            if (FishingEvent != null)
            {
                FishingEvent(type);
                Console.WriteLine("本次钓鱼结束--------------------");
            }
        }
    }

    //钓鱼者
    public class FishingMan
    {
        public string Name { get; set; }

        public int FishCount { get; set; }
        public FishingMan(string name)
        {
            Name = name;
        }

        public FishingRod FishingRod { get; set; }

        public void Fishing()
        {
            this.FishingRod.ThrowHook(this);
        }

        /// <summary>
        /// 订阅者1
        /// </summary>
        /// <param name="type"></param>
        public void Update(FishType type)
        {
            FishCount++;
            Console.WriteLine("{0}:调到第一条鱼【{2}】,已经调到{1}条鱼了", Name, FishCount, type);
        }

        /// <summary>
        /// 订阅者2
        /// </summary>
        /// <param name="type"></param>
        public void Log(FishType type)
        {
            Console.WriteLine("这是钓鱼日志:{0}", type);
        }
    }

    /// <summary>
    /// 枚举鱼的类型
    /// </summary>
    public enum FishType
    {
        鲫鱼,
        鲑鱼,
        和玉,
        黑鱼,
        鲈鱼,
        鱿鱼
    }
}
